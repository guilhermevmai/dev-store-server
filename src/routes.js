import { Router } from "express";

//? Importando a controladora
import HelloController from "./controllers/HelloController";

//? Criando o objeto de rotas;
const routes = new Router();

//? Criando a rota para o metodo GET na url '/hello' onde exibirá o retorno do método hello da controladora
routes.get('/hello', HelloController.hello);

export default routes;