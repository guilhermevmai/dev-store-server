
//? Criando controladora de exemplo

class HelloController {

    //? Metodo de teste
    hello(req, res) {
        return res.json({
            hello: "world"
        });
    }
}

export default new HelloController();